# Group Bulk Operations

This module creates an Action plugin that can assign/update a user's
group role for multiple groups in once.

When you are managing a site with many groups, it could be painful
to update a user's role for multiple groups, e.g. giving a user admin
role in tens of groups, which requires changing configurations
repeatedly for each group . This module provides the capability to
update a user's roles for as many groups as you want in one time.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/group_bulk_operations).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/group_bulk_operations).

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Requirements

This module requires the following modules:

- [Group](https://www.drupal.org/project/group)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This is a module currently serving Views only, no special configuration
required. Usage as below:

1. Add "`Bulk Update`" field to group list view
2. Ensure "`Assign group role`" is in Action selected
3. Select the group that the user's role to be changed on the group list
   view page, apply the action

## Maintainers

- Siva Karthik Reddy Papudippu - [sivakarthik229](https://www.drupal.org/u/sivakarthik229)
- wangshy - [wangshy](https://www.drupal.org/u/wangshy)
- Dhananjay Ambhure - [jayambhure1](https://www.drupal.org/u/jayambhure1)
