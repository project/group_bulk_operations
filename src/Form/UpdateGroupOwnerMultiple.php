<?php

namespace Drupal\group_bulk_operations\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a update group owner configure form.
 */
class UpdateGroupOwnerMultiple extends FormBase {

  /**
   * The array of groups to relate the user to.
   *
   * @var string[][]
   */
  protected $groupInfo = [];

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The group storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypemanager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The group storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupStorage
   */
  protected $groupStorage;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $user;

  /**
   * Constructs a AssignGroupRoleMultiple form object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    MessengerInterface $messenger
    ) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypemanager = $entity_type_manager;
    $this->groupStorage = $this->entityTypemanager->getStorage('group');
    $this->user = $this->entityTypemanager->getStorage('user');
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_bulk_operations_update_group_owner_configure';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->groupInfo = $this->tempStoreFactory->get('group_bulk_operations_update_group_owner_configure')->get($this->currentUser->id());
    if (empty($this->groupInfo)) {
      return new RedirectResponse($this->cancelForm()->setAbsolute()->toString());
    }
    /** @var \Drupal\group\Entity\GroupInterface[] $groups */
    $groups = $this->groupStorage->loadMultiple(array_keys($this->groupInfo));
    $items = [];
    foreach ($this->groupInfo as $id => $langcodes) {
      foreach ($langcodes as $langcode) {
        $group = $groups[$id]->getTranslation($langcode);
        $selected_group_type = $groups[$id]->getGroupType()->label();
        $key = $id . ':' . $langcode;
        $items[$key] = '[' . $selected_group_type . '] ' . $group->label();
      }
    }

    $form['group_bulk_heading'] = [
      '#type' => 'details',
      '#title' => $this->t('Update Owner'),
      '#open' => TRUE,
    ];

    $form['group_bulk_heading']['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('User'),
      '#description' => $this->t('Enter a user name.'),
      '#tags' => TRUE,
      '#required' => TRUE,
    ];
    $form['group_bulk_heading']['groups'] = [
      '#title' => $this->t('Groups'),
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Update Group Owner'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel_url'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#button_type' => 'button',
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    $form['#attached']['library'][] = 'group_bulk_operations/group_bulk_operations';

    return $form;
  }

  /**
   * Batch operations.
   *
   * Processes a single pending task as part of a batch operation.
   *
   * @param object $user
   *   User object.
   * @param object $group
   *   Group object.
   * @param array|\ArrayAccess $context
   *   The context of the current batch, as defined in the @link batch Batch
   *   operations @endlink documentation.
   */
  public function groupBulkOperationsBatchUpdateOwner($user, $group, &$context) {
    // Update the Owner of the group.
    $group->setOwnerId($user);
    $group->save();

    $context['results']['count'] = !array_key_exists('count', $context['results']) ? 1 : ++$context['results']['count'];

  }

  /**
   * Finishes an "execute tasks" batch.
   *
   * @param bool $success
   *   Indicates whether the batch process was successful.
   * @param array $results
   *   Results information passed from the processing callback.
   * @param array $operations
   *   Operations array from batch.
   */
  public function groupBulkOperationsBatchUpdateOwnerFinished($success, array $results, array $operations) {
    if ($success) {
      if ($results['count'] == 1) {
        $this->messenger->addStatus($this->t('@count Group is processed.', ['@count' => $results['count']]));
      }
      else {
        $this->messenger->addStatus($this->t('@count Groups are processed.', ['@count' => $results['count']]));
      }
    }
    else {
      // Error. $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $this->messenger->addError(
        $this->t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->groupInfo)) {
      $groups = $this->groupStorage->loadMultiple(array_keys($this->groupInfo));
      $groupUser = $form_state->getValue('user');
      $groupUser = reset($groupUser);
      $user = $this->user->load($groupUser['target_id']);

      if ($groups) {
        // Batch operations setting.
        $operations = [];
        foreach ($groups as $group) {
          $operations[] = [
            [$this, 'groupBulkOperationsBatchUpdateOwner'],
            [
              (int) $groupUser['target_id'],
              $group,
            // $roles[$group->getGroupType()->id()],
            ],
          ];
        }
        $batch = [
          'title' => $this->t('Updating @user as Owner to Groups', ['@user' => $user->getDisplayName()]),
          'operations' => $operations,
          'finished' => [$this, 'groupBulkOperationsBatchUpdateOwnerFinished'],
        ];

        batch_set($batch);
      }
      $this->tempStoreFactory->get('group_bulk_operations_update_group_owner_configure')->delete($this->currentUser->id());
    }
    $form_state->setRedirect('entity.group.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm() {
    return new Url('entity.group.collection');
  }

}
