<?php

namespace Drupal\group_bulk_operations\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a group multiple role assignment configure form.
 */
class AssignGroupRoleMultiple extends FormBase {

  /**
   * The array of groups to relate the user to.
   *
   * @var string[][]
   */
  protected $groupInfo = [];

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The group storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypemanager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The group storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupStorage
   */
  protected $groupStorage;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $user;

  /**
   * Constructs a AssignGroupRoleMultiple form object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    MessengerInterface $messenger
    ) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypemanager = $entity_type_manager;
    $this->groupStorage = $this->entityTypemanager->getStorage('group');
    $this->user = $this->entityTypemanager->getStorage('user');
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_bulk_operations_multiple_group_assign_role_configure';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->groupInfo = $this->tempStoreFactory->get('group_bulk_operations_multiple_group_assign_role_configure')->get($this->currentUser->id());
    if (empty($this->groupInfo)) {
      return new RedirectResponse($this->cancelForm()->setAbsolute()->toString());
    }
    /** @var \Drupal\group\Entity\GroupInterface[] $groups */
    $groups = $this->groupStorage->loadMultiple(array_keys($this->groupInfo));
    $group_roles_options = [];
    foreach ($groups as $group) {
      $group_type = $group->getGroupType();
      $internal_roles = $group_type->getRoles(FALSE);
      // phpcs:ignore
      foreach ($internal_roles as $role_id => $internal_role) {
        $group_roles_options[$internal_role->id()] = '[' . $group_type->label() . '] ' . $internal_role->label();
      }
    }
    $items = [];
    foreach ($this->groupInfo as $id => $langcodes) {
      foreach ($langcodes as $langcode) {
        $group = $groups[$id]->getTranslation($langcode);
        $selected_group_type = $groups[$id]->getGroupType()->label();
        $key = $id . ':' . $langcode;
        $items[$key] = '[' . $selected_group_type . '] ' . $group->label();
      }
    }

    $form['group_bulk_heading'] = [
      '#type' => 'details',
      '#title' => $this->t('Assign Group Role'),
      '#open' => TRUE,
    ];

    $form['group_bulk_heading']['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('User'),
      '#description' => $this->t('Enter a comma separated list of user names.'),
      '#tags' => TRUE,
      '#required' => TRUE,
    ];
    $form['group_bulk_heading']['group_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Group Roles'),
      '#options' => $group_roles_options,
      '#required' => TRUE,
    ];
    $form['group_bulk_heading']['groups'] = [
      '#title' => $this->t('Groups'),
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Assign group roles'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel_url'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#button_type' => 'button',
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    $form['#attached']['library'][] = 'group_bulk_operations/group_bulk_operations';

    return $form;
  }

  /**
   * Batch operations.
   *
   * Processes a single pending task as part of a batch operation.
   *
   * @param object $user
   *   User object.
   * @param object $group
   *   Group object.
   * @param array $roles
   *   Roles that are getting assigned to the user.
   * @param array|\ArrayAccess $context
   *   The context of the current batch, as defined in the @link batch Batch
   *   operations @endlink documentation.
   */
  public function groupBulkOperationsBatchAddMember($user, $group, array $roles, &$context) {

    // If user is not member of group, add directly, otherwise remove first.
    if (!$group->getMember($user)) {
      $group->addMember($user, ['group_roles' => $roles]);
    }
    else {
      $group->removeMember($user);
      $group->addMember($user, ['group_roles' => $roles]);
    }

    $context['results']['count'] = !array_key_exists('count', $context['results']) ? 1 : ++$context['results']['count'];

  }

  /**
   * Finishes an "execute tasks" batch.
   *
   * @param bool $success
   *   Indicates whether the batch process was successful.
   * @param array $results
   *   Results information passed from the processing callback.
   * @param array $operations
   *   Operations array from batch.
   */
  public function groupBulkOperationsBatchAddMemberFinished($success, array $results, array $operations) {
    if ($success) {
      if ($results['count'] == 1) {
        $this->messenger->addStatus($this->t('@count Group is processed.', ['@count' => $results['count']]));
      }
      else {
        $this->messenger->addStatus($this->t('@count Groups are processed.', ['@count' => $results['count']]));
      }
    }
    else {
      // Error. $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $this->messenger->addError(
        $this->t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->groupInfo)) {
      $groups = $this->groupStorage->loadMultiple(array_keys($this->groupInfo));
      $groupUsers = $form_state->getValue('user');
      $roles = [];
      foreach (array_filter($form_state->getValue('group_roles')) as $role) {
        $roles[$this->entityTypemanager->getStorage('group_role')->load($role)->getGroupType()->id()][] = $role;
      }
      // Adding multiple users to multiple groups using bulk action.
      foreach ($groupUsers as $groupUser) {
        $user = $this->user->load($groupUser['target_id']);

        if ($groups) {
          // Batch operations setting.
          $operations = [];
          foreach ($groups as $group) {
            $operations[] = [
              [$this, 'groupBulkOperationsBatchAddMember'],
              [
                $user,
                $group,
                $roles[$group->getGroupType()->id()],
              ],
            ];
          }
          $batch = [
            'title' => $this->t('Adding selected user(s) to Groups'),
            'operations' => $operations,
            'finished' => [$this, 'groupBulkOperationsBatchAddMemberFinished'],
          ];

          batch_set($batch);
        }
      }
      $this->tempStoreFactory->get('group_bulk_operations_multiple_group_assign_role_configure')->delete($this->currentUser->id());
    }
    $form_state->setRedirect('entity.group.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm() {
    return new Url('entity.group.collection');
  }

}
