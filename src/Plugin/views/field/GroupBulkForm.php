<?php

namespace Drupal\group_bulk_operations\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a group operations bulk form element.
 *
 * @ViewsField("group_bulk_form")
 */
class GroupBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No group selected.');
  }

}
