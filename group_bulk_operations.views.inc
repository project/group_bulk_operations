<?php

/**
 * @file
 * Views hooks for group_bulk_operations.
 */

/**
 * Implements hook_views_data().
 *
 * Adds group_bulk_form views field.
 */
function group_bulk_operations_views_data() {
  $data['groups']['group_bulk_form'] = [
    'title' => t('Group operations bulk form'),
    'help' => t('Add a form element that lets you run operations on multiple groups.'),
    'field' => [
      'id' => 'group_bulk_form',
    ],
  ];
  return $data;
}
